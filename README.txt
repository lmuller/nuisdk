PROJECT INFORMATION
===================
nuiSDK - Natural User Interface SDK
Project website: http://code.google.com/p/nuisdk/

Copyright (C) 2010 Laurence Muller
E-mail: laurence.muller@gmail.com
Website: http://www.multigesture.net

LICENSE
=======
GNU Lesser General Public License v3 ( http://www.gnu.org/licenses/ )

NOTES
=====
See nuiExample for more information. The nuiExample application accepts TUIO input from the simulator or any other TUIO provider.

For now the nuiExample only builds under MinGW ( http://nuisdk.googlecode.com/files/DevKit-MinGW5.7z / http://code.google.com/p/nuisdk/wiki/DevKit) with Eclipse.
After building the nuiExample in release mode, unzip the bin.7z file to the Release directory.

NEWS
====
21/02/2010 - v0.1
- Early beta of the nuiSDK

TODO
====
- nuiScatterItem needs more work
- more example widgets (image, video, browser, pdf, other osg examples)
- move the framework into a standalone dll/shared library (src\nuisdk and include\nuisdk)
-- In the current situation you can only link statically (kinda) which forces you to use the same license (LGPL/GPL) for YOUR application.
