/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 Note:
 This class is based on Touchlib's AS3 RotatableScalable class by whitenoise
 http://code.google.com/p/touchlib/source/browse/trunk/AS3/int/app/core/action/RotatableScalable.as
*/

#ifndef NUISCATTERITEM_H_
#define NUISCATTERITEM_H_

#include <osg/Group>
#include <osg/Math>
#include <osg/PositionAttitudeTransform>

#include <nuisdk/graphics/nuiTouchable.h>

#include <vector>
#include <iostream>

namespace nuisdk {

class nuiPoint
{
	public:
		nuiPoint(double _x = 0, double _y = 0)
		{
			x = _x;
			y = _y;
		}
		~nuiPoint() {}

		double x;
		double y;
};

struct touchObjectExtra
{
	long id;
	double origX;
	double origY;
	double myOrigX;
	double myOrigY;
};

struct Event
{
	int EventType;
	void (*fptr)(const touchObject &e, void *parent);
	void *_parent;
};

class nuiScatterItem;
class nuiScatterItem : public nuiTouchable, public osg::PositionAttitudeTransform // public osg::MatrixTransform
{
	public:
		enum
		{
			NONE = 0,
			DRAGGING = 1,
			ROTATESCALE = 2
		};

		// TODO: Actually this should be defined elsewhere and matching with the touchObject types
		enum
		{
			TOUCH_DOWN = 0,
			TOUCH_MOVE = 1,
			TOUCH_UP = 2,
			TOUCH_UPDATE = 3
		};

		nuiScatterItem();
		~nuiScatterItem();

		double getAngleTrig(double X, double Y);
		nuiPoint interpolate(nuiPoint pt1, nuiPoint pt2, double f);
		nuiPoint subtract(nuiPoint pt1, nuiPoint pt2);
		double distance(nuiPoint pt1, nuiPoint pt2);

		osg::Group*	getCanvas(osg::Group* curNode);
		nuiScatterItem* getParent2();

		nuiPoint globalToLocal(nuiPoint point);
		nuiPoint localToGlobal(nuiPoint point);

		void setRegistration(double x, double y);
		void setRotation2(double value);
		void setScale2(double value);

		double getx2();
		void setx2(double value);
		double gety2();
		void sety2(double value);

		void addEventListener(int EventType, void (*fptr)(const touchObject &e, void *parent));
		void removeEventListener(void (*fptr)(const touchObject &e, void *parent));

		bool downEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI);
		bool moveEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI);
		bool upEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI);

		nuiPoint getObjectById(long id, const std::vector <touchObject> &TouchAPI);

		void addBlob(const long &id, double origX, double origY, const std::vector <touchObject> &TouchAPI);
		void removeBlob(const long &id, const std::vector <touchObject> &TouchAPI);

		bool downEventTangible(const fiducialObject &e);
		bool moveEventTangible(const fiducialObject &e);
		bool upEventTangible(const fiducialObject &e);

		void update(const std::vector <touchObject> &TouchAPI);	// Call this function on every frame

		void bringToFront();

		std::vector < Event > 						_eventList;

	protected:
		bool										_update;

		int 										_state;
		bool 										_noScale;
		bool 										_noMove;
		bool 										_noRotate;

		bool										_isVisible;
		bool										_bringToFront;

		double 										_dX;
		double 										_dY;
		double 										_dAng;
		double 										_dcoef;

		double 										_curScale;
		double 										_curAngle;
		nuiPoint 									_curPosition;

		nuiPoint 									_rp;

		std::vector < touchObjectExtra >			_blobs;
		touchObjectExtra 							_blob1;
		touchObjectExtra 							_blob2;

		double 										GRAD_PI;
		double 										GRAD_PI2;
};

}

#endif /* NUISCATTERITEM_H_ */
