/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUITESTQUAD_H_
#define NUITESTQUAD_H_

#include <osg/Geode>
#include <osg/Geometry>
#include <nuisdk/graphics/nuiScatterItem.h>

namespace nuisdk {

class nuiTestQuad : public nuiScatterItem
{
	public:
		nuiTestQuad(const std::string &name);
		~nuiTestQuad();

		static void customDown(const touchObject &e, void *parent);
		static void customMove(const touchObject &e, void *parent);
		static void customUp(const touchObject &e, void *parent);
		static void customUpdate(const touchObject &e, void *parent);

		void buildSimpleQuad(double x, double y, double width, double height);

	private:
		osg::ref_ptr<osg::Geode> 		_geode;
		osg::ref_ptr<osg::Geometry> 	_geometry;
};

}

#endif /* NUITESTQUAD_H_ */
