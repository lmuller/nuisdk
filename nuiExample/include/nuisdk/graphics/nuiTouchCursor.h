/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUITOUCHCURSOR_H_
#define NUITOUCHCURSOR_H_

#include <osg/Geode>
#include <osg/Geometry>
#include <osg/PositionAttitudeTransform>

#include <nuisdk/multitouch/nuiTouchData.h>

#include <vector>

namespace nuisdk {

class nuiTouchCursor : public osg::PositionAttitudeTransform
{
	public:
		enum
		{
			NONE = 0,
			SQUARE = 1,
			CIRCLE = 2,
			CIRCLE_FILLED = 3
		};

		nuiTouchCursor();
		~nuiTouchCursor();

		void setCursorType(int type);
		void update(const std::vector <touchObject> &touchList, const std::vector <fiducialObject> &fiducialList);

	private:
		void drawCursor(const touchObject& tcur);
		void drawObject(const fiducialObject& tobj);

		void drawSquareCursor(double x, double y, double width, double height, osg::Vec4 color);
		void drawCircleCursor(double x, double y, double radius, osg::Vec4 color);
		void drawCircleFilledCursor(double x, double y, double radius, osg::Vec4 color);

		osg::ref_ptr<osg::Geode> 		_geode;
		int								_type;
		unsigned int					_segements;
		double							_deg2rad;
		float							_deginrad;
};

}

#endif /* NUITOUCHCURSOR_H_ */
