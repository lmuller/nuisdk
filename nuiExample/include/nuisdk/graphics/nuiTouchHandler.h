/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUITOUCHHANDLER_H_
#define NUITOUCHHANDLER_H_

#include <osgViewer/ViewerEventHandlers>

#include <nuisdk/multitouch/nuiTouchBridge.h>
#include <nuisdk/multitouch/nuiTouchData.h>

#include <vector>

namespace nuisdk {

class nuiTouchBridge;
class nuiTouchHandler : public osgGA::GUIEventHandler
{
	public:
		enum
		{
			UNKNOWN = 0,
			MOUSE = 1,
			MULTITOUCH = 2,
			TANGIBLE
		};

		nuiTouchHandler();
		~nuiTouchHandler();

		bool setupTUIO(int port, bool debug);


		// Get event, mouse or multitouch (TUIO)
		bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa);
		void pick(osgViewer::View* view, int type, touchObject e);
		void pick(osgViewer::View* view, int type, fiducialObject e);

		nuiTouchBridge *				_tuioBridge;	// TODO: clean up
		nuiTouchController *			_tuio;			// TODO: clean up

	private:
		std::vector <touchObject> 		_activeTouchList;
};

}

#endif /* NUITOUCHHANDLER_H_ */
