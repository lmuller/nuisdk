/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUITOUCHABLE_H_
#define NUITOUCHABLE_H_

#include <nuisdk/multitouch/nuiTouchData.h>

#include <vector>

namespace nuisdk {

class nuiTouchable
{
	public:
		nuiTouchable();
		~nuiTouchable();

		virtual bool downEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI) = 0;
		virtual bool moveEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI) = 0;
		virtual bool upEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI) = 0;

		virtual bool downEventTangible(const fiducialObject &e) = 0;
		virtual bool moveEventTangible(const fiducialObject &e) = 0;
		virtual bool upEventTangible(const fiducialObject &e) = 0;

		virtual void update(const std::vector <touchObject> &TouchAPI) = 0;
};

}

#endif /* NUITOUCHABLE_H_ */
