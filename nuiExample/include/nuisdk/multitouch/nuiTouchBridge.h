/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUITOUCHBRIDGE_H_
#define NUITOUCHBRIDGE_H_

#include <osgViewer/View>

#include <nuisdk/multitouch/nuiTouchController.h>
#include <nuisdk/graphics/nuiTouchHandler.h>

namespace nuisdk {

class nuiTouchHandler;
class nuiTouchBridge
{
	public:
		nuiTouchBridge(nuiTouchController *tuio);
		~nuiTouchBridge();

		void getFiducialUpdate();
		void getTouchUpdate();

		const std::vector <touchObject>& getNewTouchList() const;
		const std::vector <fiducialObject>& getNewFiducialList() const;

		const std::vector <touchObject>& getActiveTouchList() const;
		const std::vector <fiducialObject>& getActiveFiducialList() const;

	private:
		void addTuioObject(const fiducialObject& tobj);
		void updateTuioObject(const fiducialObject& tobj);
		void removeTuioObject(const fiducialObject& tobj);

		void addTuioCursor(const touchObject& tcur);
		void updateTuioCursor(const touchObject& tcur);
		void removeTuioCursor(const touchObject& tcur);

		nuiTouchController *			_tuio;

		std::vector <touchObject> 		_newTouchList;
		std::vector <touchObject> 		_activeTouchList;

		std::vector <fiducialObject> 	_newFiducialList;
		std::vector <fiducialObject> 	_activeFiducialList;
};

}

#endif /* NUITOUCHBRIDGE_H_ */
