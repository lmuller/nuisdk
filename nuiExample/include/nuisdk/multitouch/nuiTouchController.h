/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUICONTROLLER_H_
#define NUICONTROLLER_H_

#include <vector>
#include <TuioListener.h>
#include <TuioClient.h>
#include <OpenThreads/Mutex>
#include <nuisdk/multitouch/nuiTouchData.h>

namespace nuisdk {

class nuiTouchController : public TUIO::TuioListener
{
	public:
		enum
		{
			ADD = 0,
			UPDATE = 1,
			REMOVE = 2
		};

		nuiTouchController(int port, bool verbose);
		~nuiTouchController();

		void setDisplayProperties(int screenWidth, int screenHeight);
		bool initialize();

		// TUIO
		void addTuioObject(TUIO::TuioObject *tobj);
		void updateTuioObject(TUIO::TuioObject *tobj);
		void removeTuioObject(TUIO::TuioObject *tobj);

		void addTuioCursor(TUIO::TuioCursor *tcur);
		void updateTuioCursor(TUIO::TuioCursor *tcur);
		void removeTuioCursor(TUIO::TuioCursor *tcur);

		void refresh(TUIO::TuioTime frameTime);

		void getFiducialList(std::vector<fiducialObject> *fiducialList);
		void getTouchList(std::vector<touchObject> *touchList);

		void setInvertY(bool value);

	private:
		unsigned int					_screenWidth;
		unsigned int					_screenHeight;

		TUIO::TuioClient *				_tuioClient;
		OpenThreads::Mutex *			_syncTouchData;
		OpenThreads::Mutex *			_syncFiducialData;
		bool 							_verbose;

		std::vector <fiducialObject> 	_fiducialList;
		std::vector <touchObject> 		_touchList;

		bool							_invertY;
};

}

#endif /* NUICONTROLLER_H_ */
