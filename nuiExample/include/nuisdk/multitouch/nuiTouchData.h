/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUITOUCHDATA_H_
#define NUITOUCHDATA_H_

namespace nuisdk {

struct physicalObject
{
	long session_id;

	float X;
	float Y;
	float dX;
	float dY;
};

struct fiducialObject : physicalObject
{
	int type;
	int fiducial_id;

	float angle;
	float rotation_speed;
	float rotation_accel;
};

struct touchObject : physicalObject
{
	int type;
	int finger_id;
};

}

#endif /* NUITOUCHDATA_H_ */
