/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUISDK_H_
#define NUISDK_H_

	// SDK properties
	#define NUISDK_VERSION			"0.1"
	#define NUISDK_NAME				"nuiSDK"
	#define NUISDK_COPYRIGHT 		"2010"

	// Debug settings
	#define NUISDK_VERBOSE			1

	// Renderbins used in OSG
	#define BIN_MULTITOUCHCURSOR	1

#endif /* NUISDK_H_ */
