/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUIAPPLICATION_H_
#define NUIAPPLICATION_H_

#include <osg/Camera>
#include <osg/Group>
#include <osgDB/WriteFile>
#include <osgGA/TrackballManipulator>
#include <osgUtil/Optimizer>
#include <osgViewer/CompositeViewer>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <nuisdk/graphics/nuiTouchable.h>
#include <nuisdk/graphics/nuiTouchCursor.h>
#include <nuisdk/graphics/nuiTouchHandler.h>

#include <vector>

namespace nuisdk {

void getScreenSize(unsigned int *w, unsigned int *h);

class nuiTouchApplication
{
	public:
		nuiTouchApplication(unsigned int screenWidth, unsigned int screenHeight, bool fullscreen);
		~nuiTouchApplication();

		bool setupTUIO(int port, bool debug);
		bool setupOpenSceneGraph(bool optimize);

		void saveSceneGraph(const std::string &filename);

		void start();
		void stop();

		void addEventListener(nuiTouchable* eventListener);
		void removeEventListener(nuiTouchable* eventListener);

	protected:
		virtual bool initialize() = 0;
		virtual void frame() = 0;
		void updateEvents();

		std::vector <nuiTouchable*>						_eventListener;
		osg::ref_ptr<osgViewer::CompositeViewer>		_compositeViewer;
		osg::ref_ptr<osgViewer::Viewer>					_viewer;
		osg::ref_ptr<osg::Group> 						_root;
		osg::ref_ptr<nuiTouchHandler>					_TouchHandler;
		unsigned int									_screenWidth;
		unsigned int									_screenHeight;
		bool											_fullscreen;
		osg::ref_ptr<nuiTouchCursor>					_cursors;

	private:
		bool 											_isRunning;
		int												_tuioPort;
		bool											_tuioDebug;
};

}

#endif /* NUIAPPLICATION_H_ */
