/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nuiExample.h"
#include <stdio.h>

int main(int argc, char **argv)
{

// Step 1: Create a class based on nuiTouchApplication

	// Automatic Screen Resolution
	//unsigned int screen_width, screen_height;
	//nuisdk::getScreenSize(&screen_width, &screen_height);
	//nuiExample *nuiapp = new nuiExample(screen_width, screen_width, true);

	// Manual Screen Resolution
	nuiExample *nuiapp = new nuiExample(1024, 768, false);	// Press f to go fullscreen or use the 'hack' in nuiTouchApplication.cpp

// Step 2: Build the scenegraph
	if(!nuiapp->setupOpenSceneGraph(false))
		return -1;

// Step 3: Setup the TUIO connection for multitouch input
	if(!nuiapp->setupTUIO(3333, false))
		return -2;

// Step 4: Start the application
	nuiapp->start();

// Stop 5: Stop the application (can be used from a function)
	nuiapp->stop();

// Step 6: Clean up the mess you just made :p
	delete nuiapp;
	nuiapp = 0;

	return 0;
}
