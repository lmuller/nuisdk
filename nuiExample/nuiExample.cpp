/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nuiExample.h"
#include <stdio.h>

#include <nuisdk/graphics/nuiTestQuad.h>

nuiExample::nuiExample(unsigned int screenWidth, unsigned int screenHeight, bool fullscreen) : nuiTouchApplication(screenWidth, screenHeight, fullscreen)
{
	// empty, input parameters forwarded to parent class
}

nuiExample::~nuiExample()
{
	// empty
}

bool nuiExample::initialize()
{
	// Add canvas to scene
	_canvas = new nuiCanvas();
		_root->addChild(_canvas.get());
		addEventListener(_canvas.get());

	// Add objects to canvas
	osg::ref_ptr<nuiTestQuad> _quad1 = new nuiTestQuad("Quad1");
		_quad1->buildSimpleQuad(10, 10, 40, 40);
		addEventListener(_quad1.get());

	osg::ref_ptr<nuiTestQuad> _quad2 = new nuiTestQuad("Quad2");
		_quad2->buildSimpleQuad(640, 480, 80, 80);
		_canvas->addChild(_quad2.get());
		addEventListener(_quad2.get());
		_quad2->insertChild(0, _quad1.get());

	osg::ref_ptr<nuiTestQuad> _quad3 = new nuiTestQuad("Quad3");
		_quad3->buildSimpleQuad(30, 30, 200, 200);
		_canvas->addChild(_quad3.get());
		addEventListener(_quad3.get());

	return true;
}

void nuiExample::frame()
{
	// Add custom stuff here (Processing events, add objects, etc.)
}
