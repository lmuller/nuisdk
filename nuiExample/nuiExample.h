/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NUIEXAMPLE_H_
#define NUIEXAMPLE_H_

#include <nuisdk/graphics/nuiCanvas.h>
#include <nuisdk/nuiTouchApplication.h>

using namespace nuisdk;

class nuiExample : public nuiTouchApplication
{
	public:
		nuiExample(unsigned int screenWidth, unsigned int screenHeight, bool fullscreen);
		~nuiExample();

	protected:
		bool initialize();
		void frame();

	private:
		osg::ref_ptr<nuiCanvas>		_canvas;
};

#endif /* NUIEXAMPLE_H_ */
