/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 Note:
 This class is based on Touchlib's AS3 RotatableScalable class by whitenoise
 http://code.google.com/p/touchlib/source/browse/trunk/AS3/int/app/core/action/RotatableScalable.as
*/

#include <nuisdk/nuiSDK.h>
#include <nuisdk/graphics/nuiScatterItem.h>
#include <stdio.h>

namespace nuisdk {

nuiScatterItem::nuiScatterItem()
{
	// Set default state of scatter item
	_state = nuiScatterItem::NONE;
	_blobs.clear();

	// Set properties
	_noScale = false;
	_noMove = false;
	_noRotate = false;

	// TODO: Fixme
	_isVisible = true;
	_bringToFront = true;

	// Set values
	_dX = 0;
	_dY = 0;
	_dAng = 0;
	_dcoef = 0.5;

	_rp.x = 0;
	_rp.y = 0;

	// Store constants
	GRAD_PI = 180.0f / osg::PI;
	GRAD_PI2 = osg::PI / 180.0f;

	// Set position matrix
	this->setPosition(osg::Vec3(0.0, 0.0, 0.0));
}

nuiScatterItem::~nuiScatterItem()
{
	// Empty
}

// Borrowed from touchlib to figure out the angle
double nuiScatterItem::getAngleTrig(double X, double Y)
{
	if (X == 0.0f)
	{
		if(Y < 0.0f)
			return 270.0f;
		else
			return 90.0f;
	}
	else if (Y == 0.0f)
	{
		if(X < 0.0f)
			return 180.0f;
		else
			return 0.0f;
	}

	if ( Y > 0.0f)
		if (X > 0.0f)
			return atan(Y/X) * GRAD_PI;
		else
			return 180.0f - atan(Y/-X) * GRAD_PI;
	else
		if (X > 0.0f)
			return 360.0f - atan(-Y/X) * GRAD_PI;
		else
			return 180.0f + atan(-Y/-X) * GRAD_PI;
}

nuiPoint nuiScatterItem::interpolate(nuiPoint pt1, nuiPoint pt2, double f)
{
	nuiPoint pt3;
	pt3.x = (pt1.x - (pt1.x - pt2.x) * f);
	pt3.y = (pt1.y - (pt1.y - pt2.y) * f);

	return pt3;
}

nuiPoint nuiScatterItem::subtract(nuiPoint pt1, nuiPoint pt2)
{
	nuiPoint tmp;
	tmp.x = pt1.x - pt2.x;
	tmp.y = pt1.y - pt2.y;

	return tmp;
}

double nuiScatterItem::distance(nuiPoint pt1, nuiPoint pt2)
{
	return sqrt(((pt1.x - pt2.x) * (pt1.x - pt2.x))	+ ((pt1.y - pt2.y) * (pt1.y - pt2.y)));
}

osg::Group*	nuiScatterItem::getCanvas(osg::Group* curNode)
{
	osg::Group *parent = curNode->getParent(0);
	if(parent->getName().compare("Canvas") == 0)
		return parent;
	else
		return getCanvas(parent);
}

nuiScatterItem* nuiScatterItem::getParent2()
{
	osg::Group *p = this->getParent(0);
	nuiScatterItem* parent = dynamic_cast<nuiScatterItem *>(p);

	return parent;
}

// Convert point object from World (global) coordinates to display object's local coordinates.
nuiPoint nuiScatterItem::globalToLocal(nuiPoint point)
{
	// Get world (Global) matrix
	osg::MatrixList ml = this->getWorldMatrices();

	// Remove relative local offset
	double x = ml[0](3, 0) - point.x;
	double y = ml[0](3, 1) - point.y;

	// Return final local position
	return nuiPoint(x, y);
}

// Converts the point object from display object's (local) coordinates to the World (global) coordinates.
nuiPoint nuiScatterItem::localToGlobal(nuiPoint point)
{
	// Get world (Global) matrix
	osg::MatrixList ml = this->getWorldMatrices();

	// Add relative local offset
	double x = ml[0](3, 0) + point.x;
	double y = ml[0](3, 1) + point.y;

	// Return final global position
	return nuiPoint(x, y);
}

void nuiScatterItem::setRegistration(double x, double y)
{
	_rp.x = x;
	_rp.y = y;
}

// Todo: the next two functions need work, what is the purpose of a and b
void nuiScatterItem::setRotation2(double value)
{
	nuiPoint a(0,0);
	nuiPoint b(0,0);

	if(this->getParent2())
	{
		a = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
		osg::Quat curRot = this->getAttitude();
		this->setAttitude(osg::Quat(curRot.x(), curRot.y(), value, curRot.w()));
		b = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
	}
	else
	{
		a = this->localToGlobal(_rp);
		osg::Quat curRot = this->getAttitude();
		this->setAttitude(osg::Quat(curRot.x(), curRot.y(), value, curRot.w()));
		b = this->localToGlobal(_rp);
	}

	double x = this->getPosition().x();
	double y = this->getPosition().y();
	x -= b.x - a.x;
	y -= b.y - a.y;
	this->setPosition(osg::Vec3(x, y, this->getPosition().z()));
}

void nuiScatterItem::setScale2(double value)
{
	nuiPoint a(0,0);
	nuiPoint b(0,0);

	if(this->getParent2())
	{
		a = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
		this->setScale(osg::Vec3(value, value, 1.0));
		b = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
	}
	else
	{
		a = this->localToGlobal(_rp);
		this->setScale(osg::Vec3(value, value, 1.0));
		b = this->localToGlobal(_rp);
	}

	double x = this->getPosition().x();
	double y = this->getPosition().y();
	x -= b.x - a.x;
	y -= b.y - a.y;
	this->setPosition(osg::Vec3(x, y, this->getPosition().z()));
}

double nuiScatterItem::getx2()
{
	nuiPoint p = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
	return p.x;
}

void nuiScatterItem::setx2(double value)
{
	nuiPoint p = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
	double x = this->getPosition().x();
	x += value - p.x;
	this->setPosition(osg::Vec3(x, this->getPosition().y(), this->getPosition().z()));
}

double nuiScatterItem::gety2()
{
	nuiPoint p = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
	return p.y;
}

void nuiScatterItem::sety2(double value)
{
	nuiPoint p = this->getParent2()->globalToLocal(this->localToGlobal(_rp));
	double y = this->getPosition().y();
	y += value - p.y;
	this->setPosition(osg::Vec3(this->getPosition().x(), y, this->getPosition().z()));
}

void nuiScatterItem::addEventListener(int EventType, void (*fptr)(const touchObject &e, void *parent))
{
	nuisdk::Event e;
	e.EventType = EventType;
	e.fptr = fptr;
	e._parent = this;
	_eventList.push_back(e);
}

void nuiScatterItem::removeEventListener(void (*fptr)(const touchObject &e, void *parent))
{
	for(unsigned int i = 0; i < _eventList.size(); ++i)
	{
		if(_eventList[i].fptr == fptr)
		{
			// Remove touchObject from local blobs list
			_eventList.erase(_eventList.begin() + i);
			return;
		}
	}
}

bool nuiScatterItem::downEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI)
{
	nuiScatterItem *parent = this->getParent2();
	if(!parent)
		return false;

	nuiPoint curPt = parent->globalToLocal(nuiPoint(e.X, e.Y));

	addBlob(e.session_id, curPt.x, curPt.y, TouchAPI);

	// Bring to front code here
	if(_bringToFront)
		bringToFront();


	// Process events
	for(unsigned int i = 0; i < _eventList.size(); ++i)
	{
		if(_eventList[i].EventType == nuiScatterItem::TOUCH_DOWN)
			_eventList[i].fptr(e, _eventList[i]._parent);
	}

	// Stop propagation
	return true;
}

bool nuiScatterItem::moveEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI)
{
	// Process events
	for(unsigned int i = 0; i < _eventList.size(); ++i)
	{
		if(_eventList[i].EventType == nuiScatterItem::TOUCH_MOVE)
			_eventList[i].fptr(e, _eventList[i]._parent);
	}

	return true;	// Stop propagation
}

bool nuiScatterItem::upEvent(const touchObject &e, const std::vector <touchObject> &TouchAPI)
{
	removeBlob(e.session_id, TouchAPI);

	// Process events
	for(unsigned int i = 0; i < _eventList.size(); ++i)
	{
		if(_eventList[i].EventType == nuiScatterItem::TOUCH_UP)
			_eventList[i].fptr(e, _eventList[i]._parent);
	}

	return true;	// Stop propagation
}

nuiPoint nuiScatterItem::getObjectById(long id, const std::vector <touchObject> &TouchAPI)
{
	nuiPoint tmp(-1.0, -1.0);

	for(unsigned int i = 0; i < TouchAPI.size(); ++i)
	{
		if(TouchAPI[i].session_id == id)
		{
			tmp.x = TouchAPI[i].X;
			tmp.y = TouchAPI[i].Y;

			return tmp;
		}
	}

	return tmp;
}

void nuiScatterItem::addBlob(const long &id, double origX, double origY, const std::vector <touchObject> &TouchAPI)
{
	nuiScatterItem *parent = this->getParent2();
	if(!parent)
		return;

	for(unsigned int i = 0; i < _blobs.size(); ++i)
	{
		if(_blobs[i].id == id)
			return;
	}

	// Get current position/scale/rotation from matrix
	osg::Vec3 curPos = this->getPosition();
	osg::Vec3 curScale = this->getScale();
	osg::Quat curRot = this->getAttitude();

	// Create new object and store in the local bloblist
	touchObjectExtra tmp;
	tmp.id = id;
	tmp.origX = origX;
	tmp.origY = origY;
	tmp.myOrigX = curPos.x();
	tmp.myOrigY = curPos.y();
	_blobs.push_back(tmp);

	if(_blobs.size() == 1)
	{
		_state = nuiScatterItem::DRAGGING;

		_curScale = curScale.x();
		_curAngle = curRot.z();
		_curPosition.x = curPos.x();
		_curPosition.y = curPos.y();

		_blob1 = _blobs[0];
	}
	else if(_blobs.size() == 2)
	{
		_state = nuiScatterItem::ROTATESCALE;

		_curScale = curScale.x();
		_curAngle = curRot.z();
		_curPosition.x = curPos.x();
		_curPosition.y = curPos.y();

		_blob1 = _blobs[0];
		_blob2 = _blobs[1];

		nuiPoint tuioobj1 = getObjectById(_blob1.id, TouchAPI);
		nuiPoint tuioobj2 = getObjectById(_blob2.id, TouchAPI);

		nuiPoint midPoint = interpolate(	this->globalToLocal(tuioobj1),
											this->globalToLocal(tuioobj2),
											0.5);

		setRegistration(midPoint.x, midPoint.y);

		if(tuioobj1.x != -1.0)
		{
			nuiPoint curPt1 = parent->globalToLocal(tuioobj1);
			_blob1.origX = curPt1.x;
			_blob1.origY = curPt1.y;
		}
	}
	else
	{
		_state = nuiScatterItem::DRAGGING;
		//_state = nuiScatterItem::ROTATESCALE;	// ?

		return;
	}
}

void nuiScatterItem::removeBlob(const long &id, const std::vector <touchObject> &TouchAPI)
{
	nuiScatterItem *parent = this->getParent2();
	if(!parent)
		return;

	// Get current position/scale/rotation from matrix
	osg::Vec3 curPos = this->getPosition();
	osg::Vec3 curScale = this->getScale();
	osg::Quat curRot = this->getAttitude();

	for(unsigned int i = 0; i < _blobs.size(); ++i)
	{
		if(_blobs[i].id == id)
		{
			// Remove touchObject from local blobs list
			_blobs.erase(_blobs.begin() + i);

			if(_blobs.size() == 0)
			{
				_state = nuiScatterItem::NONE;
			}
			else if (_blobs.size() == 1)
			{
				_state = nuiScatterItem::DRAGGING;

				_curScale = curScale.x();
				_curAngle = curRot.z();
				_curPosition.x = curPos.x();
				_curPosition.y = curPos.y();

				_blob1 = _blobs[0];

				nuiPoint tuioobj1 = getObjectById(_blob1.id, TouchAPI);

				if(tuioobj1.x != -1.0)
				{
					nuiPoint curPt1 = parent->globalToLocal(tuioobj1);
					_blob1.origX = curPt1.x;
					_blob1.origY = curPt1.y;
				}
			}
			else	// >= 2
			{
				_state = nuiScatterItem::ROTATESCALE;

				_curScale = curScale.x();
				_curAngle = curRot.z();
				_curPosition.x = curPos.x();
				_curPosition.y = curPos.y();

				_blob1 = _blobs[0];
				_blob2 = _blobs[1];

				nuiPoint tuioobj1 = getObjectById(_blob1.id, TouchAPI);

				if(tuioobj1.x != -1.0)
				{
					nuiPoint curPt1 = parent->globalToLocal(tuioobj1);
					_blob1.origX = curPt1.x;
					_blob1.origY = curPt1.y;
				}
			}

			return;
		}
	}
}

bool nuiScatterItem::downEventTangible(const fiducialObject &e)
{
	return true;	// Stop propagation
}

bool nuiScatterItem::moveEventTangible(const fiducialObject &e)
{
	return true;	// Stop propagation
}

bool nuiScatterItem::upEventTangible(const fiducialObject &e)
{
	return true;	// Stop propagation
}

void nuiScatterItem::update(const std::vector <touchObject> &TouchAPI)
{
	if(_state == nuiScatterItem::DRAGGING)
	{
		nuiScatterItem *parent = this->getParent2();
		if(!parent)
			return;

		nuiPoint tuioobj1 = getObjectById(_blob1.id, TouchAPI);
		if(tuioobj1.x == -1.0)
		{
			removeBlob(_blob1.id, TouchAPI);
			return;
		}

		nuiPoint curPt = parent->globalToLocal(tuioobj1);

		double oldX = this->getPosition().x();
		double oldY = this->getPosition().y();

		if(!_noMove)
		{
			/*
			// Hmm + isn't right so switch to -. But why why does the flash implementation works with -
			this->setPosition(osg::Vec3(_curPosition.x + (curPt.x - (_blob1.origX)),
										_curPosition.y + (curPt.y - (_blob1.origY)),
										this->getPosition().z()));
			*/

			this->setPosition(osg::Vec3(_curPosition.x - (curPt.x - (_blob1.origX)),
										_curPosition.y - (curPt.y - (_blob1.origY)),
										this->getPosition().z()));
		}

		_dX *= _dcoef;
		_dY *= _dcoef;
		_dAng *= _dcoef;
		_dX += this->getPosition().x() - oldX;
		_dY += this->getPosition().y() - oldY;

	}
	else if(_state == nuiScatterItem::ROTATESCALE)
	{
		nuiScatterItem *parent = this->getParent2();
		if(!parent)
			return;

		nuiPoint tuioobj1 = getObjectById(_blob1.id, TouchAPI);
		if(tuioobj1.x == -1.0)
		{
			removeBlob(_blob1.id, TouchAPI);
			return;
		}
		nuiPoint curPt1 = parent->globalToLocal(tuioobj1);
		nuiPoint tuioobj2 = getObjectById(_blob2.id, TouchAPI);
		if(tuioobj2.x == -1.0)
		{
			removeBlob(_blob1.id, TouchAPI);
			return;
		}
		nuiPoint curPt2 = parent->globalToLocal(tuioobj2);
		nuiPoint curCenter = this->interpolate(curPt1, curPt2, 0.5);

		nuiPoint origPt1(_blob1.origX, _blob1.origY);
		nuiPoint origPt2(_blob2.origX, _blob2.origY);
		nuiPoint centerOrig = this->interpolate(origPt1, origPt2, 0.5);

		nuiPoint offs = this->subtract(curCenter, centerOrig);

		double len1 = this->distance(origPt1, origPt2);
		double len2 = this->distance(curPt1, curPt2);
//		double len3 = this->distance(origPt1, nuiPoint(0,0));

		double newscale = _curScale * len2 / len1;

		if(!_noScale)
		{
			this->setScale(osg::Vec3(newscale, newscale, 1.0));	// keep z scale 1
			//this->setScale2(newscale);
		}

		nuiPoint origLine = origPt1;
		origLine = this->subtract(origLine, origPt2);

		double ang1 = this->getAngleTrig(origLine.x, origLine.y);

		nuiPoint curLine = curPt1;
		curLine = this->subtract(curLine, curPt2);

		double ang2 = this->getAngleTrig(curLine.x, curLine.y);

		double oldAngle = this->getAttitude().z();

		if(!_noRotate)
		{
			// TODO: Check if we need to pass degrees or radians
			this->setRotation2(_curAngle + osg::DegreesToRadians(ang2 - ang1));
		}

		double oldX = this->getPosition().x();
		double oldY = this->getPosition().y();
/*
		if(!_noMove)
		{
			//this->setx2(curCenter.x);
			//this->sety2(curCenter.y);
		}
*/
		_dX *= _dcoef;
		_dY *= _dcoef;
		_dAng *= _dcoef;

//		_dX += this->getPosition().x() - oldX;
//		_dY += this->getPosition().y() - oldY;

//		_dAng += this->getAttitude().z() - oldAngle;
	}
	else
	{
		if(_dX != 0 || _dY != 0)
		{
			_dX = 0;
			_dY = 0;
			_dAng = 0;
		}
	}

	// Process events
	touchObject e;	// Just a null object
	e.type = e.session_id = e.finger_id = e.X = e.Y = e.dX = e.dY = 0;	// Initialize with rubbish (not used)
	for(unsigned int i = 0; i < _eventList.size(); ++i)
	{
		if(_eventList[i].EventType == nuiScatterItem::TOUCH_UPDATE)
			_eventList[i].fptr(e, _eventList[i]._parent);
	}
}

void nuiScatterItem::bringToFront()
{
	return;
}

}
