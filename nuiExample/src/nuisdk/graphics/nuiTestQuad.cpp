/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <nuisdk/nuiSDK.h>
#include <nuisdk/graphics/nuiTestQuad.h>
#include <stdio.h>

namespace nuisdk {

nuiTestQuad::nuiTestQuad(const std::string &name)
{
	// Set this name
	this->setName(name);

	// Create objects
	_geode = new osg::Geode;
	_geometry = new osg::Geometry();

	// Set properties
	_geometry->setUseDisplayList(false);

	// Use VertexBuffer Objects for the geometry
	_geometry->setUseVertexBufferObjects(true);
	_geometry->setDataVariance(osg::Object::STATIC);

	// Connect objects
	_geode->addDrawable(_geometry.get());
	this->addChild(_geode.get());

	addEventListener(nuiScatterItem::TOUCH_DOWN, &customDown);
	//addEventListener(nuiScatterItem::TOUCH_MOVE, &customMove);
	addEventListener(nuiScatterItem::TOUCH_UP, &customUp);
	//addEventListener(nuiScatterItem::TOUCH_UPDATE, &customUpdate);
}

nuiTestQuad::~nuiTestQuad()
{
	// empty;
}

void nuiTestQuad::customDown(const touchObject &e, void *parent)
{
	nuiTestQuad* _this = (nuiTestQuad*)parent;	// '_this' is 'this'
	printf("customDown (%d)\n", (int)e.session_id);
}

void nuiTestQuad::customMove(const touchObject &e, void *parent)
{
	nuiTestQuad* _this = (nuiTestQuad*)parent;	// '_this' is 'this'
	printf("customMove (%d)\n", (int)e.session_id);
}

void nuiTestQuad::customUp(const touchObject &e, void *parent)
{
	nuiTestQuad* _this = (nuiTestQuad*)parent;	// '_this' is 'this'
	printf("customUp (%d)\n", (int)e.session_id);
}

void nuiTestQuad::customUpdate(const touchObject &e, void *parent)
{
	nuiTestQuad* _this = (nuiTestQuad*)parent;	// '_this' is 'this'
	printf("customUpdate\n");
}

// Todo set scale and rotation
void nuiTestQuad::buildSimpleQuad(double x, double y, double width, double height)
{
	// Build the object around the 0,0 coordinate
	double halfx = width / 2;
	double halfy = height / 2;

	// Add vertices
	osg::ref_ptr<osg::Vec3Array> verts_node = new osg::Vec3Array;
	verts_node->push_back(osg::Vec3( halfx,  halfy, 0));	// top right
	verts_node->push_back(osg::Vec3(-halfx,  halfy, 0));	// top left
	verts_node->push_back(osg::Vec3(-halfx, -halfy, 0));	// bottom left
	verts_node->push_back(osg::Vec3( halfx, -halfy, 0));	// bottom right

	// Add vertex colors
	osg::ref_ptr<osg::Vec4Array> colors_node = new osg::Vec4Array;
	colors_node->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f) ); //index 0 red
	colors_node->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f) ); //index 1 green
	colors_node->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f) ); //index 2 blue
	colors_node->push_back(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f) ); //index 3 white

	// Bind
	_geometry->setVertexArray(verts_node.get());
	_geometry->setColorArray(colors_node.get());
	_geometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
	_geometry->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4));

	// Set the position of the quad by adjusting the Matrix
	this->setPosition(osg::Vec3(x, y, 0.0));
}

}
