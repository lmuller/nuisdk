/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <osg/Hint>
#include <osg/MatrixTransform>
#include <nuisdk/nuiSDK.h>
#include <nuisdk/graphics/nuiTouchCursor.h>
#include <stdio.h>
#include <math.h>

namespace nuisdk {

nuiTouchCursor::nuiTouchCursor()
{
	this->setName("nuiTouchCursor");
	this->setReferenceFrame(osg::Transform::ABSOLUTE_RF);

	_geode = new osg::Geode;
	this->addChild(_geode.get());

	_type = nuiTouchCursor::CIRCLE;

	// Circle Cursor
	_segements = 12;
	_deg2rad = 2 * 3.14159 / _segements;
}

nuiTouchCursor::~nuiTouchCursor()
{
	// empty
}

void nuiTouchCursor::setCursorType(int type)
{
	_type = type;

	if(_type == nuiTouchCursor::NONE)
	{
		// Remove cursors from last frame
		_geode->removeDrawables(0, _geode->getNumDrawables());
	}
}

void nuiTouchCursor::update(const std::vector <touchObject> &touchList, const std::vector <fiducialObject> &fiducialList)
{
	if(_type == nuiTouchCursor::NONE)
		return;

	// Remove cursors from last frame
	_geode->removeDrawables(0, _geode->getNumDrawables());

	// Go draw new cursors
	unsigned int listsize = (unsigned int)touchList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		drawCursor(touchList[i]);
	}

	listsize = (unsigned int)fiducialList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		drawObject(fiducialList[i]);
	}
}

void nuiTouchCursor::drawCursor(const touchObject& tcur)
{
	double x = tcur.X;
	double y = tcur.Y;
	double width = 20.;
	double height = 20.;
	double radius = 15.;
	osg::Vec4 color(1.0f, 0.0f, 0.0f, 1.0f);

	switch(_type)
	{
		case nuiTouchCursor::SQUARE:
			drawSquareCursor(x, y, width, height, color);
		break;

		case nuiTouchCursor::CIRCLE:
			drawCircleCursor(x, y, radius, color);
		break;

		case nuiTouchCursor::CIRCLE_FILLED:
			drawCircleFilledCursor(x, y, radius, color);
		break;

		default:
			drawSquareCursor(x, y, width, height, color);
	}
}

void nuiTouchCursor::drawObject(const fiducialObject& tobj)
{
	double x = tobj.X;
	double y = tobj.Y;
	double width = 20;
	double height = 20;
	osg::Vec4 color(0.0f, 1.0f, 0.0f, 1.0f);

	drawSquareCursor(x, y, width, height, color);
}

void nuiTouchCursor::drawSquareCursor(double x, double y, double width, double height, osg::Vec4 color)
{
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	osg::ref_ptr<osg::Vec3Array> verts = new osg::Vec3Array;
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;

	width /= 2;
	height /= 2;

	verts->push_back(osg::Vec3(x - width, y + height, 0.0));
	verts->push_back(osg::Vec3(x - width, y - height, 0.0));
	verts->push_back(osg::Vec3(x + width, y - height, 0.0));
	verts->push_back(osg::Vec3(x + width, y + height, 0.0));

	colors->push_back(color);
	colors->push_back(color);
	colors->push_back(color);
	colors->push_back(color);

	geom->setVertexArray(verts.get());
	geom->setColorArray(colors.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, 0, 4));

	_geode->addDrawable(geom.get());
}

void nuiTouchCursor::drawCircleCursor(double x, double y, double radius, osg::Vec4 color)
{
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	osg::StateSet* stateset = geom->getOrCreateStateSet();
	stateset->setMode(GL_BLEND, osg::StateAttribute::ON);
	stateset->setMode(GL_LINE_SMOOTH, osg::StateAttribute::ON);
	osg::ref_ptr<osg::Hint> hint = new osg::Hint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	stateset->setAttribute(hint.get());
	osg::ref_ptr<osg::Vec3Array> verts = new osg::Vec3Array;
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;

	for(unsigned int i = 0; i < _segements; ++i)
	{
		_deginrad = i * _deg2rad;
		verts->push_back(osg::Vec3(x + cos(_deginrad) * radius, y + sin(_deginrad) * radius, 0.0));
		colors->push_back(color);
	}

	geom->setVertexArray(verts.get());
	geom->setColorArray(colors.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::LINE_LOOP, 0, _segements));

	_geode->addDrawable(geom.get());
}

void nuiTouchCursor::drawCircleFilledCursor(double x, double y, double radius, osg::Vec4 color)
{
	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
	osg::ref_ptr<osg::Vec3Array> verts = new osg::Vec3Array;
	osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;

	for(unsigned int i = 0; i < _segements; ++i)
	{
		_deginrad = i * _deg2rad;
		verts->push_back(osg::Vec3(x + cos(_deginrad) * radius, y + sin(_deginrad) * radius, 0.0));
		colors->push_back(color);
	}

	geom->setVertexArray(verts.get());
	geom->setColorArray(colors.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::POLYGON, 0, _segements));

	_geode->addDrawable(geom.get());
}

}
