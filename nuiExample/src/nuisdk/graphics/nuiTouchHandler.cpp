/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <nuisdk/nuiSDK.h>
#include <nuisdk/graphics/nuiTouchHandler.h>
#include <nuisdk/multitouch/nuiTouchController.h>
#include <nuisdk/graphics/nuiScatterItem.h>

namespace nuisdk {

nuiTouchHandler::nuiTouchHandler()
{
	_tuio = 0;
	_tuioBridge = 0;
}

nuiTouchHandler::~nuiTouchHandler()
{
	// Clean up
	if(_tuio)
		delete _tuio;

	_tuio = 0;

	if(_tuioBridge)
		delete _tuioBridge;

	_tuioBridge = 0;
}

bool nuiTouchHandler::setupTUIO(int port, bool debug)
{
	_tuio = new nuiTouchController(port, debug);
	if(!_tuio->initialize())
	{
		printf("[TUIO] Error: Could not initialize tuioClient!\n");
		return false;
	}
	else
	{
		_tuioBridge = new nuiTouchBridge(_tuio);
		return true;
	}
}

bool nuiTouchHandler::handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter& aa)
{
	osgViewer::View * view = dynamic_cast<osgViewer::View*>(&aa);

	// Process multitouch events
	_tuioBridge->getTouchUpdate();	// Update Active Touch List
	std::vector <touchObject> touchList = _tuioBridge->getNewTouchList();
	_activeTouchList = _tuioBridge->getActiveTouchList();

	unsigned int listsize = (unsigned int)touchList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		pick(view, nuiTouchHandler::MULTITOUCH, touchList[i]);
	}

	// Process fiducial events
	_tuioBridge->getFiducialUpdate();	// Update Active Fiducial List
	std::vector <fiducialObject> fiducialList = _tuioBridge->getNewFiducialList();

	listsize = (unsigned int)fiducialList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		pick(view, nuiTouchHandler::TANGIBLE, fiducialList[i]);
	}

	// Process mouse events
	switch(ea.getEventType())
	{
		// Mouse Click
		case(osgGA::GUIEventAdapter::PUSH):
		{
			if (view)
			{
				// Fake
				touchObject e;
				e.X = ea.getX();
				e.Y = ea.getY();
				e.dX = 0;
				e.dY = 0;
				e.finger_id = -1;
				e.session_id = -1;
				e.type= nuiTouchController::ADD;

				pick(view, nuiTouchHandler::MOUSE, e);
			}
			return false;
		}
		break;

		case(osgGA::GUIEventAdapter::RELEASE):
		{
			if (view)
			{
				// Fake
				touchObject e;
				e.X = ea.getX();
				e.Y = ea.getY();
				e.dX = 0;
				e.dY = 0;
				e.finger_id = -1;
				e.session_id = -1;
				e.type= nuiTouchController::REMOVE;

				pick(view, nuiTouchHandler::MOUSE, e);
			}
			return false;
		}
		break;

		default:
			return false;
	}
}

void nuiTouchHandler::pick(osgViewer::View* view, int type, touchObject e)
{
	osgUtil::LineSegmentIntersector::Intersections intersections;

	switch(type)
	{
		case nuiTouchHandler::MOUSE:
			printf("pick: %d x %d\n", (int)(e.X), (int)(e.Y));
			if (view->computeIntersections(e.X, e.Y, intersections))
			{
				for(	osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
						hitr != intersections.end();
						++hitr)
				{
					nuiTouchable *touchableObject = dynamic_cast<nuiTouchable *>(hitr->nodePath.back()->getParent(0));

					nuiScatterItem *scatterObject = dynamic_cast<nuiScatterItem *>(touchableObject);
					if(scatterObject)
					{
						printf("pick: %s\n", scatterObject->getName().c_str());
					}

					if(touchableObject)
					{
						switch(e.type)
						{
							case nuiTouchController::ADD:
								if(touchableObject->downEvent(e, _activeTouchList))
									return;
							break;

							case nuiTouchController::UPDATE:
								//if(touchableObject->moveEvent(e, _activeTouchList))
									return;
							break;

							case nuiTouchController::REMOVE:
								if(touchableObject->upEvent(e, _activeTouchList))
									return;
							break;

							default:
								printf("[nuiSDK] Warning: Unknown type at nuiTouchHandler::pick (%d)\n", e.type);
						}
					}
				}
			}
		break;

		case nuiTouchHandler::MULTITOUCH:
		{
			if (view->computeIntersections(e.X, e.Y, intersections))
			{
				for(	osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
						hitr != intersections.end();
						++hitr)
				{
					nuiTouchable *touchableObject = dynamic_cast<nuiTouchable *>(hitr->nodePath.back()->getParent(0));
					if(touchableObject)
					{
						switch(e.type)
						{
							case nuiTouchController::ADD:
								if(touchableObject->downEvent(e, _activeTouchList))
									return;
							break;

							case nuiTouchController::UPDATE:
								//if(touchableObject->moveEvent(e, _activeTouchList))
									return;
							break;

							case nuiTouchController::REMOVE:
								if(touchableObject->upEvent(e, _activeTouchList))
									return;
							break;

							default:
								printf("[nuiSDK] Warning: Unknown type at nuiTouchHandler::pick (%d)\n", e.type);
						}
					}
				}
			}
		}
		break;
	}
}

void nuiTouchHandler::pick(osgViewer::View* view, int type, fiducialObject e)
{
	osgUtil::LineSegmentIntersector::Intersections intersections;

	if (view->computeIntersections(e.X, e.Y, intersections))
	{
		for(	osgUtil::LineSegmentIntersector::Intersections::iterator hitr = intersections.begin();
				hitr != intersections.end();
				++hitr)
		{
			nuiTouchable *touchableObject = dynamic_cast<nuiTouchable *>(hitr->nodePath.back()->getParent(0));
			if(touchableObject)
			{
				switch(e.type)
				{
					case nuiTouchController::ADD:
						if(touchableObject->downEventTangible(e))
							return;
					break;

					case nuiTouchController::UPDATE:
						//if(touchableObject->moveEventTangible(e))
							return;
					break;

					case nuiTouchController::REMOVE:
						if(touchableObject->upEventTangible(e))
							return;
					break;

					default:
						printf("[nuiSDK] Warning: Unknown type at nuiTouchHandler::pick (%d) Tangible\n", e.type);
				}
			}
		}
	}
}

}
