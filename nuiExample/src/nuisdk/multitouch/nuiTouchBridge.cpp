/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <nuisdk/nuiSDK.h>
#include <nuisdk/multitouch/nuiTouchBridge.h>
#include <stdio.h>

namespace nuisdk {

nuiTouchBridge::nuiTouchBridge(nuiTouchController *tuio)
{
	_tuio = tuio;
}

nuiTouchBridge::~nuiTouchBridge()
{
#if defined(NUISDK_VERBOSE)
	printf("[nuiSDK] Message: Shutting down TUIO Bridge\n");
#endif
	_tuio = 0;
}



void nuiTouchBridge::getFiducialUpdate()
{
	// Give an empty list to the TUIOController and let it will with fiducial data
	_newFiducialList.clear();
	_tuio->getFiducialList(&_newFiducialList);

	unsigned int listsize = (unsigned int)_newFiducialList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		switch(_newFiducialList[i].type)
		{
			case nuiTouchController::ADD:
				addTuioObject(_newFiducialList[i]);
			break;

			case nuiTouchController::UPDATE:
				updateTuioObject(_newFiducialList[i]);
			break;

			case nuiTouchController::REMOVE:
				removeTuioObject(_newFiducialList[i]);
			break;

			default:
				printf("[nuiSDK] Warning: Unknown type at getFiducialUpdate()\n");
		}
	}
}

void nuiTouchBridge::getTouchUpdate()
{
	// Give an empty list to the TUIOController and let it will with touch data
	_newTouchList.clear();
	_tuio->getTouchList(&_newTouchList);

	unsigned int listsize = (unsigned int)_newTouchList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		switch(_newTouchList[i].type)
		{
			case nuiTouchController::ADD:
				addTuioCursor(_newTouchList[i]);
			break;

			case nuiTouchController::UPDATE:
				updateTuioCursor(_newTouchList[i]);
			break;

			case nuiTouchController::REMOVE:
				removeTuioCursor(_newTouchList[i]);
			break;

			default:
				printf("[nuiSDK] Warning: Unknown type at getTouchUpdate()\n");
		}
	}
}

const std::vector <touchObject>& nuiTouchBridge::getNewTouchList() const
{
	return _newTouchList;
}

const std::vector <fiducialObject>& nuiTouchBridge::getNewFiducialList() const
{
	return _newFiducialList;
}

const std::vector <touchObject>& nuiTouchBridge::getActiveTouchList() const
{
	return _activeTouchList;
}

const std::vector <fiducialObject>& nuiTouchBridge::getActiveFiducialList() const
{
	return _activeFiducialList;
}

void nuiTouchBridge::addTuioObject(const fiducialObject& tobj)
{
	// Add tangible object to current fiducial list
	fiducialObject _tobj = tobj;
	_activeFiducialList.push_back(_tobj);
}

void nuiTouchBridge::updateTuioObject(const fiducialObject& tobj)
{
	// Update tangible object in current fiducial list
	fiducialObject _tobj = tobj;
	unsigned int listsize = (unsigned int)_activeFiducialList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		if(_activeFiducialList[i].session_id == tobj.session_id)
		{
			_activeFiducialList[i] = _tobj;
			break;
		}
	}
}

void nuiTouchBridge::removeTuioObject(const fiducialObject& tobj)
{
	// Remove tangible object from fiducial list
	unsigned int listsize = (unsigned int)_activeFiducialList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		if(_activeFiducialList[i].session_id == tobj.session_id)
		{
			_activeFiducialList.erase(_activeFiducialList.begin() + i);
			break;
		}
	}
}

void nuiTouchBridge::addTuioCursor(const touchObject& tcur)
{
	// Add touch to _activerent touch list
	touchObject _tcur = tcur;
	_activeTouchList.push_back(_tcur);
}

void nuiTouchBridge::updateTuioCursor(const touchObject& tcur)
{
	// Update touch in current touch list
	touchObject _tcur = tcur;
	unsigned int listsize = (unsigned int)_activeTouchList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		if(_activeTouchList[i].session_id == tcur.session_id)
		{
			_activeTouchList[i] = _tcur;
			break;
		}
	}
}

void nuiTouchBridge::removeTuioCursor(const touchObject& tcur)
{
	// Remove touch from touch list
	unsigned int listsize = (unsigned int)_activeTouchList.size();
	for (unsigned int i = 0; i < listsize; ++i)
	{
		if(_activeTouchList[i].session_id == tcur.session_id)
		{
			_activeTouchList.erase(_activeTouchList.begin() + i);
			break;
		}
	}
}

}
