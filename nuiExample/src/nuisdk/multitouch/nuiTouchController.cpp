/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <nuisdk/nuiSDK.h>
#include <nuisdk/multitouch/nuiTouchController.h>
#include <stdio.h>

namespace nuisdk {

nuiTouchController::nuiTouchController(int port, bool verbose)
{
	_verbose = verbose;
#if defined(NUISDK_VERBOSE)
	printf("[nuiSDK] Message: ");
#endif
	_tuioClient = new TUIO::TuioClient(port);
	_tuioClient->addTuioListener(this);

	_syncTouchData = new OpenThreads::Mutex();
	_syncFiducialData = new OpenThreads::Mutex();

	_invertY = true;
}

nuiTouchController::~nuiTouchController()
{
#if defined(NUISDK_VERBOSE)
	printf("[nuiSDK] Message: Shutting down TUIO controller\n");
#endif
	_tuioClient->disconnect();

	delete _syncFiducialData;
	delete _syncTouchData;
	delete _tuioClient;
}

void nuiTouchController::setDisplayProperties(int screenWidth, int screenHeight)
{
	_screenWidth = screenWidth;
	_screenHeight = screenHeight;
}

bool nuiTouchController::initialize()
{
	if (!_tuioClient->isConnected())
	{
		_tuioClient->connect();
	}
	else
		printf("[TUIO] Warning: Already connected\n");

	if (!_tuioClient->isConnected())
	{
		printf("[TUIO] Error: Could not connect tuioClient!\n");
		return false;
	}

	return true;
}

void nuiTouchController::addTuioObject(TUIO::TuioObject *tobj)
{
	if(_verbose)
		std::cout << "[TUIO] add obj " << tobj->getSymbolID() << " (" << tobj->getSessionID() << ") "<< tobj->getX() << " " << tobj->getY() << " " << tobj->getAngle() << std::endl;

	fiducialObject tmp;
	tmp.type = nuiTouchController::ADD;
	tmp.angle = tobj->getAngle();
	tmp.fiducial_id = tobj->getSymbolID();
	tmp.rotation_accel = tobj->getRotationAccel();
	tmp.rotation_speed = tobj->getRotationSpeed();
	tmp.session_id = tobj->getSessionID();

	tmp.X = tobj->getX();
	if(_invertY)
		tmp.Y = (float)1.0 - tobj->getY();
	else
		tmp.Y = tobj->getY();

	tmp.dX = 0;
	tmp.dY = 0;

	tmp.X *= _screenWidth;
	tmp.Y *= _screenHeight;

	_syncFiducialData->lock();
	_fiducialList.push_back(tmp);
	_syncFiducialData->unlock();
}

void nuiTouchController::updateTuioObject(TUIO::TuioObject *tobj)
{
	if(_verbose)
		std::cout << "[TUIO] set obj " << tobj->getSymbolID() << " (" << tobj->getSessionID() << ") "<< tobj->getX() << " " << tobj->getY() << " " << tobj->getAngle()
				<< " " << tobj->getMotionSpeed() << " " << tobj->getRotationSpeed() << " " << tobj->getMotionAccel() << " " << tobj->getRotationAccel() << std::endl;

	fiducialObject tmp;
	tmp.type = nuiTouchController::UPDATE;
	tmp.angle = tobj->getAngle();
	tmp.fiducial_id = tobj->getSymbolID();
	tmp.rotation_accel = tobj->getRotationAccel();
	tmp.rotation_speed = tobj->getRotationSpeed();
	tmp.session_id = tobj->getSessionID();

	tmp.X = tobj->getX();
	if(_invertY)
		tmp.Y = (float)1.0 - tobj->getY();
	else
		tmp.Y = tobj->getY();

	tmp.dX = 0;
	tmp.dY = 0;

	tmp.X *= _screenWidth;
	tmp.Y *= _screenHeight;

	_syncFiducialData->lock();
	_fiducialList.push_back(tmp);
	_syncFiducialData->unlock();
}

void nuiTouchController::removeTuioObject(TUIO::TuioObject *tobj)
{
	if(_verbose)
		std::cout << "[TUIO] del obj " << tobj->getSymbolID() << " (" << tobj->getSessionID() << ")" << std::endl;

	fiducialObject tmp;
	tmp.type = nuiTouchController::REMOVE;
	tmp.angle = tobj->getAngle();
	tmp.fiducial_id = tobj->getSymbolID();
	tmp.rotation_accel = tobj->getRotationAccel();
	tmp.rotation_speed = tobj->getRotationSpeed();
	tmp.session_id = tobj->getSessionID();

	tmp.X = tobj->getX();
	if(_invertY)
		tmp.Y = (float)1.0 - tobj->getY();
	else
		tmp.Y = tobj->getY();

	tmp.dX = 0;
	tmp.dY = 0;

	tmp.X *= _screenWidth;
	tmp.Y *= _screenHeight;

	_syncFiducialData->lock();
	_fiducialList.push_back(tmp);
	_syncFiducialData->unlock();
}

void nuiTouchController::addTuioCursor(TUIO::TuioCursor *tcur)
{
	if(_verbose)
		std::cout << "[TUIO] add cur " << tcur->getCursorID() << " (" <<  tcur->getSessionID() << ") " << tcur->getX() << " " << tcur->getY() << std::endl;

	touchObject tmp;
	tmp.type = nuiTouchController::ADD;
	tmp.finger_id = tcur->getCursorID();
	tmp.session_id = tcur->getSessionID();

	tmp.X = tcur->getX();
	if(_invertY)
		tmp.Y = (float)1.0 - tcur->getY();
	else
		tmp.Y = tcur->getY();

	tmp.dX = 0;
	tmp.dY = 0;

	tmp.X *= _screenWidth;
	tmp.Y *= _screenHeight;

	_syncTouchData->lock();
	_touchList.push_back(tmp);
	_syncTouchData->unlock();
}

void nuiTouchController::updateTuioCursor(TUIO::TuioCursor *tcur)
{
	if(_verbose)
		std::cout << "[TUIO] set cur " << tcur->getCursorID() << " (" <<  tcur->getSessionID() << ") " << tcur->getX() << " " << tcur->getY()
				<< " " << tcur->getMotionSpeed() << " " << tcur->getMotionAccel() << " " << std::endl;

	touchObject tmp;
	tmp.type = nuiTouchController::UPDATE;
	tmp.finger_id = tcur->getCursorID();
	tmp.session_id = tcur->getSessionID();

	tmp.X = tcur->getX();
	if(_invertY)
		tmp.Y = (float)1.0 - tcur->getY();
	else
		tmp.Y = tcur->getY();

	tmp.dX = 0;
	tmp.dY = 0;

	tmp.X *= _screenWidth;
	tmp.Y *= _screenHeight;

	_syncTouchData->lock();
	_touchList.push_back(tmp);
	_syncTouchData->unlock();
}

void nuiTouchController::removeTuioCursor(TUIO::TuioCursor *tcur)
{
	if(_verbose)
			std::cout << "[TUIO] del cur " << tcur->getCursorID() << " (" <<  tcur->getSessionID() << ")" << std::endl;

	touchObject tmp;
	tmp.type = nuiTouchController::REMOVE;
	tmp.finger_id = tcur->getCursorID();
	tmp.session_id = tcur->getSessionID();

	tmp.X = tcur->getX();
	if(_invertY)
		tmp.Y = (float)1.0 - tcur->getY();
	else
		tmp.Y = tcur->getY();

	tmp.dX = 0;
	tmp.dY = 0;

	tmp.X *= _screenWidth;
	tmp.Y *= _screenHeight;

	_syncTouchData->lock();
	_touchList.push_back(tmp);
	_syncTouchData->unlock();
}

void nuiTouchController::refresh(TUIO::TuioTime frameTime)
{
	// not used
}

void nuiTouchController::getFiducialList(std::vector<fiducialObject> *fiducialList)
{
	_syncFiducialData->lock();
	(*fiducialList) = _fiducialList;	// Copy active fiducial markers
	_fiducialList.clear();				// Clear local list
	_syncFiducialData->unlock();
}

void nuiTouchController::getTouchList(std::vector<touchObject> *touchList)
{
	_syncTouchData->lock();
	(*touchList) = _touchList;	// Copy active touches
	_touchList.clear();			// Clear local list
	_syncTouchData->unlock();
}

void nuiTouchController::setInvertY(bool value)
{
	_invertY = value;
}

}
