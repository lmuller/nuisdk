/*
	nuiSDK - Natural User Interface SDK
	Project website: http://code.google.com/p/nuisdk/

	Copyright (C) 2010 Laurence Muller
	E-mail: laurence.muller@gmail.com
	Website: http://www.multigesture.net

	This file is part of the nuiSDK.

	The nuiSDK is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	The nuiSDK is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with the nuiSDK.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <nuisdk/nuiSDK.h>
#include <nuisdk/nuiTouchApplication.h>
#include <stdio.h>

namespace nuisdk {

void getScreenSize(unsigned int *w, unsigned int *h)
{
	osg::GraphicsContext::WindowingSystemInterface* wsi = osg::GraphicsContext::getWindowingSystemInterface();
	wsi->getScreenResolution(osg::GraphicsContext::ScreenIdentifier(0), (*w), (*h));	// Using display 0 (== first)
	printf("[nuiExample] Message: Screen resolution: %d x %d (Display 0)\n", (*w), (*h));
}


nuiTouchApplication::nuiTouchApplication(unsigned int screenWidth, unsigned int screenHeight, bool fullscreen)
{
	srand(time(NULL));

	_screenWidth = screenWidth;
	_screenHeight = screenHeight;
	_fullscreen = fullscreen;
	_isRunning = false;

	printf("Using "NUISDK_NAME" version: " NUISDK_VERSION "\n");
	printf("Copyright (C) "NUISDK_COPYRIGHT" Laurence Muller\n\n");
}

nuiTouchApplication::~nuiTouchApplication()
{
	// empty
}

bool nuiTouchApplication::setupTUIO(int port, bool debug)
{
	printf("[nuiSDK] Message: Setup TUIO\n");

	_tuioPort = port;
	_tuioDebug = debug;
	_TouchHandler = new nuiTouchHandler();
	bool success = _TouchHandler->setupTUIO(_tuioPort, _tuioDebug);

	if(success)
	{
		// Add touchhandler (includes mouse picking) to event handler list.
		_viewer->addEventHandler(_TouchHandler.get());
		_TouchHandler->_tuio->setDisplayProperties(_screenWidth, _screenHeight);
	}
	else
	{
		printf("[nuiSDK] Error: failed to setup TUIO");
	}

	return success;
}

bool nuiTouchApplication::setupOpenSceneGraph(bool optimize)
{
	printf("[nuiSDK] Message: Setup OpenSceneGraph\n");

	_compositeViewer = new osgViewer::CompositeViewer();
	_viewer = new osgViewer::Viewer();
	_compositeViewer->addView(_viewer.get());

	_root = new osg::Group();
	_root->setName("Root");

	// Be green and save some energy... turn off the lights ;)...
	_root->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::OFF);

	// Turn depth testing off and use renderbins!
	//_root->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::OFF);

	// Setup window mode
	if(!_fullscreen)
		_viewer->setUpViewInWindow(32, 32, _screenWidth, _screenHeight);

	// Initialize user scene, e.g.: widgets, background, GUI. Attach to root.
	if(!initialize())
		return false;

	// Optimize the scene graph, remove redundant/empty nodes and state etc.
	if(optimize)
	{
		osgUtil::Optimizer optimizer;
		optimizer.optimize(_root.get());
	}

	// Add cursors to canvas
	_cursors = new nuiTouchCursor();
	_root->insertChild(0, _cursors.get());

	// Setup orthographic projection mode (2D)
	osg::Camera* camera = _viewer->getCamera();
	camera->setName("Camera1");
	camera->setViewport(new osg::Viewport(0, 0, _screenWidth, _screenHeight));
	camera->setProjectionMatrix(osg::Matrix::ortho2D(0.0, _screenWidth, 0.0, _screenHeight));
	camera->setReferenceFrame(osg::Transform::ABSOLUTE_RF);
	camera->setViewMatrix(osg::Matrix::identity());

	// Attach root to viewer
	_viewer->setSceneData(_root.get());

	// Add eventhandlers
	//_viewer->setCameraManipulator(new osgGA::TrackballManipulator());	// Useful for debug, doesn't works proper with ortho2d enabled
	_viewer->addEventHandler(new osgViewer::StatsHandler);
	_viewer->addEventHandler(new osgViewer::HelpHandler);
	_viewer->addEventHandler(new osgViewer::WindowSizeHandler);
	_viewer->addEventHandler(new osgViewer::ThreadingHandler);

	return true;
}

void nuiTouchApplication::saveSceneGraph(const std::string &filename)
{
	printf("[nuiSDK] Message: Saving scene to %s\n", filename.c_str());

	// Write scene to a file
	osgDB::writeNodeFile(*_viewer->getCamera(), filename);
}

void nuiTouchApplication::start()
{
	printf("[nuiSDK] Message: Starting nuiTouchApplication\n");

	_viewer->realize();
	_isRunning = true;

	// TODO: Fixme, this is a workaround to start in fullscreen mode
	//_viewer->getEventQueue()->keyPress('f');
	//_viewer->getEventQueue()->keyRelease('f');

	// Main render loop
	while(!_compositeViewer->done() && _isRunning)
	{
		// Update application frame
		frame();

		// Render frame
		_compositeViewer->frame();

		// Update cursors
		this->_cursors->update(_TouchHandler->_tuioBridge->getActiveTouchList(), _TouchHandler->_tuioBridge->getActiveFiducialList());
		this->updateEvents();
	}

	_isRunning = false;
}

void nuiTouchApplication::stop()
{
	printf("[nuiSDK] Message: Stopping nuiTouchApplication\n");
	_isRunning = false;
}

// Used to update nuiScatterItem and nuiTouchable items.
void nuiTouchApplication::addEventListener(nuiTouchable* eventListener)
{
	_eventListener.push_back(eventListener);
}

void nuiTouchApplication::removeEventListener(nuiTouchable* eventListener)
{
	unsigned int eventListenerSize = (unsigned int)_eventListener.size();
	for (unsigned int i = 0; i < eventListenerSize; ++i)
	{
		if(_eventListener[i] == eventListener)
		{
			_eventListener.erase(_eventListener.begin() + i);
			break;
		}
	}
}

void nuiTouchApplication::updateEvents()
{
	unsigned int eventListenerSize = (unsigned int)_eventListener.size();
	for (unsigned int i = 0; i < eventListenerSize; ++i)
	{
		_eventListener[i]->update(_TouchHandler->_tuioBridge->getActiveTouchList());
	}
}

}
